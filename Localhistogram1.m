function img1 = Localhistogram1( img )
R =img(:, :, 1);
G =img(:, :, 2);
B =img(:, :, 3);
%对RGB分量进行局部直方图均衡化
IR=adapthisteq(R,'numtiles',[2 2],'clipLimit',0.02,'Distribution','rayleigh');
IG=adapthisteq(G,'numtiles',[2 2],'clipLimit',0.02,'Distribution','rayleigh');
IB=adapthisteq(B,'numtiles',[2 2],'clipLimit',0.02,'Distribution','rayleigh');
% 恢复到RGB
img1= cat(3, IR, IG, IB);
end

