function varargout = m1(varargin)
% M1 MATLAB code for m1.fig
%      M1, by itself, creates a new M1 or raises the existing
%      singleton*.
%
%      H = M1 returns the handle to a new M1 or the handle to
%      the existing singleton*.
%
%      M1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in M1.M with the given input arguments.
%
%      M1('Property','Value',...) creates a new M1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before m1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to m1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help m1

% Last Modified by GUIDE v2.5 03-Dec-2021 19:18:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @m1_OpeningFcn, ...
                   'gui_OutputFcn',  @m1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before m1 is made visible.
function m1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to m1 (see VARARGIN)

% Choose default command line output for m1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes m1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = m1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    axes(handles.axes1);
    cla reset;
    axes(handles.axes2);
    cla reset;
    axes(handles.axes3);
    cla reset;
    axes(handles.axes4);
    cla reset;
    set(handles.edit1,'string','');
[file,path]=uigetfile({'*.jpg';'*.bmp';'gif'},'输入图像');
if isequal(file,0)|isequal(path,0)
    errordlg('未选中图片','出错');
    return;
else
    img=imread([path,file]);
    axes(handles.axes1);
    imshow(img);
    title('原图','FontWeight','Bold');
end
 handles.img = img;
 guidata(hObject,handles);



% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA
pnum=get(handles.popupmenu1,'value');
switch pnum
    case 1
        handles.img1=GlobalHistogram(handles.img);%执行全局直方图均衡化去雾
        axes(handles.axes3);
        cla reset;
        axes(handles.axes4);
        cla reset;
        axes(handles.axes2);imshow(handles.img1,[]);%结果图显示在axes2
        guidata(hObject,handles);
        set(handles.edit1,'String','全局直方图算法');%将方法名显示在edit1中
%         save('handles.img1');%保存去雾后的图片
    case 2
        handles.img1=Localhistogram1(handles.img);%执行局部直方图均衡化去雾
        axes(handles.axes3);
        cla reset;
        axes(handles.axes4);
        cla reset;
        axes(handles.axes2);imshow(handles.img1,[]);%结果图显示在axes2
        guidata(hObject,handles);
        set(handles.edit1,'String','局部直方图算法');%将方法名显示在edit1中
%         save('handles.img1');%保存去雾后的图片
    case 3
        handles.img1=Retinexsuanfa(handles.img);%执行Retinex单尺度算法去雾
        axes(handles.axes3);
        cla reset;
        axes(handles.axes4);
        cla reset;
        axes(handles.axes2);imshow(handles.img1,[]);%结果图显示在axes2
        guidata(hObject,handles);
        set(handles.edit1,'String','Retinex单尺度算法');%将方法名显示在edit1中
%         save('handles.img1');%保存去雾后的图片
 case 4
        handles.img1=Retinexsuanfa1(handles.img);%执行Retinex多尺度算法去雾
        axes(handles.axes3);
        cla reset;
        axes(handles.axes4);
        cla reset;
        axes(handles.axes2);imshow(handles.img1,[]);%结果图显示在axes2
        guidata(hObject,handles);
        set(handles.edit1,'String','Retinex多尺度算法');%将方法名显示在edit1中
%         save('handles.img1');%保存去雾后的图片
    otherwise
end
title('去雾后','FontWeight','Bold');



% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img=rgb2gray(handles.img);
img1=rgb2gray(handles.img1);
axes(handles.axes3);imhist(img);title('原图灰度直方图','fontweight','bold');
axes(handles.axes4);imhist(img1);title('去雾后灰度直方图','fontweight','bold');


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
choice = questdlg('确定要退出系统?', ...
    '退出', ...
    '确定','取消','取消');
switch choice
    case '确定'
        close;
    case '取消'
        return;
end



% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
pnum=get(handles.popupmenu1,'value');
if isequal(pnum==1, ~1)
    axes(handles.axes2);
    cla reset;
    axes(handles.axes3);
    cla reset;
    axes(handles.axes4);
    cla reset;
    set(handles.edit1,'string','')
end
if isequal(pnum==2, ~2)
    axes(handles.axes2);
    cla reset;
    axes(handles.axes3);
    cla reset;
    axes(handles.axes4);
    cla reset;
    set(handles.edit1,'string','')
end
if isequal(pnum==3, ~3)
    axes(handles.axes2);
    cla reset;
    axes(handles.axes3);
    cla reset;
    axes(handles.axes4);
    cla reset;
    set(handles.edit1,'string','')
    
end
if isequal(pnum==4, ~4)
    axes(handles.axes2);
    cla reset;
    axes(handles.axes3);
    cla reset;
    axes(handles.axes4);
    cla reset;
    set(handles.edit1,'string','')
    
end
    




% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
