function img = Retinexsuanfa1( img )
R = img(:, :, 1);
%取对数将照射光分量和反射光分量分离
R0 = double(R);
Rlog = log(R0+1);
Rfft2 = fft2(R0);

G = img(:, :, 2);
G0 = double(G);
Glog = log(G0+1);
Gfft2 = fft2(G0);

B = img(:, :, 3);
B0 = double(B);
Blog = log(B0+1);
Bfft2 = fft2(B0);
%色彩恢复因子C
a=2.5;
I1=imadd(R0,G0);
I2=imadd(I1,B0);
Ir=immultiply(R0,a);
C1=imdivide(Ir,I2);
C=log(C1+1);

%定义模板大小
[N1, M1] = size(R); 
sigma1= 128;
F1= fspecial('gaussian', [N1,M1], sigma1);
Ffft1 = fft2(double(F1));
 %高斯模板和原图像作卷积，低通滤波
DR01 = Rfft2.* Ffft1;
DR1 = ifft2(DR01);
 %在对数域中，原始图像减去低通滤波后的图像，得到高频增强的图像
DRlog1 = log(DR1 +1);
Rr1 = Rlog - DRlog1;

sigma2= 256;
F2= fspecial('gaussian', [N1,M1], sigma2);
Ffft2 = fft2(double(F2));
 %高斯模板和原图像作卷积，低通滤波
DR02 = Rfft2.* Ffft2;
DR2 = ifft2(DR02);
 %在对数域中，原始图像减去低通滤波后的图像，得到高频增强的图像
DRlog2 = log(DR2 +1);
Rr2 = Rlog - DRlog2;

sigma3= 512;
F3 = fspecial('gaussian', [N1,M1], sigma3);
Ffft3 = fft2(double(F3));
 %高斯模板和原图像作卷积，低通滤波
DR03 = Rfft2.* Ffft3;
DR3 = ifft2(DR03);
 %在对数域中，原始图像减去低通滤波后的图像，得到高频增强的图像
DRlog3 = log(DR3 +1);
Rr3 = Rlog - DRlog3;

Rr=(1/3)*(Rr1+Rr2+Rr3);
Rr4=immultiply(C,Rr);
%取反对数
EXPRr = exp(Rr4);
%对增强后的图像对比度拉伸增强
MIN = min(min(EXPRr));
MAX = max(max(EXPRr));
EXPRr1 = (EXPRr - MIN)/(MAX - MIN);
EXPRr2 = adapthisteq(EXPRr1);

%定义模板大小
[N2, M2] = size(G); 
sigma4= 128;
F4= fspecial('gaussian', [N2,M2], sigma4);
Ffft4 = fft2(double(F4));
 %高斯模板和原图像作卷积，低通滤波
DG01= Gfft2.* Ffft4;
DG1 = ifft2(DG01);
DGlog1 = log(DG1 +1);
Gg1 = Glog - DGlog1;

sigma5= 256;
F5= fspecial('gaussian', [N2,M2], sigma5);
Ffft5 = fft2(double(F5));
 %高斯模板和原图像作卷积，低通滤波
DG02 = Gfft2.* Ffft5;
DG2 = ifft2(DG02);
DGlog2 = log(DG2 +1);
Gg2 = Glog - DGlog2;

sigma6= 512;
F6 = fspecial('gaussian', [N2,M2], sigma6);
Ffft6 = fft2(double(F6));
 %高斯模板和原图像作卷积，低通滤波
DG03 = Gfft2.* Ffft6;
DG3 = ifft2(DG03);
DGlog3 = log(DG3 +1);
Gg3 = Glog - DGlog3;

Gg=(1/3)*(Gg1+Gg2+Gg3);
Gg4=immultiply(C,Gg);
EXPGg = exp(Gg4);
MIN = min(min(EXPGg));
MAX = max(max(EXPGg));
EXPGg1 = (EXPGg - MIN)/(MAX - MIN);
EXPGg2 = adapthisteq(EXPGg1);

%定义模板大小
[N3, M3] = size(B); 
sigma7= 128;
F7= fspecial('gaussian', [N3,M3], sigma7);
Ffft7 = fft2(double(F7));
 %高斯模板和原图像作卷积，低通滤波
DB01 = Bfft2.* Ffft7;
DB1 = ifft2(DB01);
DBlog1 = log(DB1+1);
Bb1 = Blog - DBlog1;

sigma8= 256;
F8= fspecial('gaussian', [N3,M3], sigma8);
Ffft8 = fft2(double(F8));
 %高斯模板和原图像作卷积，低通滤波
DB02 = Bfft2.* Ffft8;
DB2 = ifft2(DB02);
DBlog2 = log(DB2+1);
Bb2 = Blog - DBlog2;

sigma9= 512;
F9 = fspecial('gaussian', [N3,M3], sigma9);
Ffft9 = fft2(double(F9));
 %高斯模板和原图像作卷积，低通滤波
DB03 = Bfft2.* Ffft9;
DB3 = ifft2(DB03);
DBlog3 = log(DB3+1);
Bb3 = Blog - DBlog3;

Bb=(1/3)*(Bb1+Bb2+Bb3);
Bb4=immultiply(C,Bb);
EXPBb = exp(Bb4);
MIN = min(min(EXPBb));
MAX = max(max(EXPBb));
EXPBb1 = (EXPBb - MIN)/(MAX - MIN);
EXPBb2 = adapthisteq(EXPBb1);
img= cat(3, EXPRr2, EXPGg2, EXPBb2);
end

