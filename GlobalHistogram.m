function img1 = GlobalHistogram( img )
r=img(:,:,1);
g=img(:,:,2);
b=img(:,:,3);
R=histeq(r);
G=histeq(g);
B=histeq(b);
img1=cat(3,R,G,B);
end

