function img = Retinexsuanfa( img )
R = img(:, :, 1);
%取对数将照射光分量和反射光分量分离
R0 = double(R);
Rlog = log(R0+1);
Rfft2 = fft2(R0);
%定义模板大小
[N1, M1] = size(R); 
sigma1 =250;
F1= fspecial('gaussian', [N1,M1], sigma1);
Ffft1 = fft2(double(F1));
 %高斯模板和原图像作卷积，低通滤波
DR0 = Rfft2.* Ffft1;
DR = ifft2(DR0);
 %在对数域中，原始图像减去低通滤波后的图像，得到高频增强的图像
DRlog = log(DR +1);
Rr = Rlog - DRlog;
%取反对数
EXPRr = exp(Rr);
%对增强后的图像对比度拉伸增强
MIN = min(min(EXPRr));
MAX = max(max(EXPRr));
EXPRr1 = (EXPRr - MIN)/(MAX - MIN);
EXPRr2 = adapthisteq(EXPRr1);
 
G = img(:, :, 2);
G0 = double(G);
Glog = log(G0+1);
Gfft2 = fft2(G0);
%定义模板大小
[N2, M2] = size(G); 
sigma2= 250;
F2= fspecial('gaussian', [N2,M2], sigma2);
Ffft2 = fft2(double(F2));
 %高斯模板和原图像作卷积，低通滤波
DG0 = Gfft2.* Ffft2;
DG = ifft2(DG0);
DGlog = log(DG +1);
Gg = Glog - DGlog;
EXPGg = exp(Gg);
MIN = min(min(EXPGg));
MAX = max(max(EXPGg));
EXPGg1 = (EXPGg - MIN)/(MAX - MIN);
EXPGg2 = adapthisteq(EXPGg1);
 
B = img(:, :, 3);
B0 = double(B);
Blog = log(B0+1);
Bfft2 = fft2(B0);
%定义模板大小
[N3, M3] = size(B); 
sigma3= 250;
F3 = fspecial('gaussian', [N3,M3], sigma3);
Ffft3 = fft2(double(F3));
 %高斯模板和原图像作卷积，低通滤波
DB0 = Bfft2.* Ffft3;
DB = ifft2(DB0);
DBlog = log(DB+1);
Bb = Blog - DBlog;
EXPBb = exp(Bb);
MIN = min(min(EXPBb));
MAX = max(max(EXPBb));
EXPBb1 = (EXPBb - MIN)/(MAX - MIN);
EXPBb2 = adapthisteq(EXPBb1);
%RGB三通道合成一副图像
img= cat(3, EXPRr2, EXPGg2, EXPBb2);
end

